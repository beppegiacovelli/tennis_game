package it.uniba.tennisgame;

import static org.junit.Assert.*;

import org.junit.Test;

public class GameTest {

	@Test //1 a 2
	public void testFifteenThirty() throws Exception {
		//Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1 = game.getPlayerName1();
		String playerName2 = game.getPlayerName2();
		
		//Act
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		//Assert
		assertEquals("Federer fifteen - Nadal thirty", game.getGameStatus());
	}

	@Test //3 a 2
	public void testFortyThirty() throws Exception {
		//Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1 = game.getPlayerName1();
		String playerName2 = game.getPlayerName2();
		
		//Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		//Assert
		assertEquals("Federer forty - Nadal thirty", game.getGameStatus());
	}
	
	@Test //4 a 2
	public void testFortyFifteen() throws Exception {
		//Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1 = game.getPlayerName1();
		String playerName2 = game.getPlayerName2();
		
		//Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		game.incrementPlayerScore(playerName1);
		
		//Assert
		assertEquals("Federer wins", game.getGameStatus());
	}

	@Test //1 a 3
	public void testFifteenForty() throws Exception {
		//Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1 = game.getPlayerName1();
		String playerName2 = game.getPlayerName2();
		
		//Act
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		//Assert
		assertEquals("Federer fifteen - Nadal forty", game.getGameStatus());
	}
	
	@Test //3 a 3
	public void testFortyForty() throws Exception {
		//Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1 = game.getPlayerName1();
		String playerName2 = game.getPlayerName2();
		
		//Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		//Assert
		assertEquals("Deuce", game.getGameStatus());
	}
	
	@Test //4 a 3
	public void testPlayerOneAdvantage() throws Exception {
		//Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1 = game.getPlayerName1();
		String playerName2 = game.getPlayerName2();
		
		//Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		game.incrementPlayerScore(playerName1);

		//Assert
		assertEquals("Advantage Federer", game.getGameStatus());
	}
	
	@Test //1 a 4
	public void testPlayerTwoWins() throws Exception {
		//Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1 = game.getPlayerName1();
		String playerName2 = game.getPlayerName2();
		
		//Act
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		//Assert
		assertEquals("Nadal wins", game.getGameStatus());
	}
	
	@Test //3 a 4
	public void testPlayerTwoAdvantage() throws Exception {
		//Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1 = game.getPlayerName1();
		String playerName2 = game.getPlayerName2();
		
		//Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		//Assert
		assertEquals("Advantage Nadal", game.getGameStatus());
	}
	
	@Test //4 a 4
	public void testDeuce() throws Exception {
		//Arrange
		Game game = new Game("Federer", "Nadal");
		String playerName1 = game.getPlayerName1();
		String playerName2 = game.getPlayerName2();
		
		//Act
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		game.incrementPlayerScore(playerName1);
		
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		game.incrementPlayerScore(playerName2);
		
		game.incrementPlayerScore(playerName1);
		
		//Assert
		assertEquals("Deuce", game.getGameStatus());
	}
	
}
